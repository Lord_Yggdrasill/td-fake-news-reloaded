<?php require_once ('bdd_connexion.php');
session_start();
require_once 'backend_nav.php';
require_once ('head.php');
if (isset($_SESSION['pseudo'])&& $_SESSION['role'] == 'admin') {?>
    <a class="expand" href="add_user.php">Ajouter un utilisateur</a>
    <?php
    $req = $bdd->query('SELECT * FROM users');
    ?>
    <h2>Utilisateurs actuels : </h2>
    <?php
    ?>
    <div class="user-flex">
    <?php
    while ($donnees = $req->fetch()) {?>
        <div class="user-flex-line">
            <div class="font-weight-bold margin_user user-fetch"><?php echo $donnees['login']; ?></div>
            <div class="user-fetch"><a href="modify_user.php?id=<?php echo $donnees['id'] ?>">modifier</a></div>
            <div class="user-fetch"><a href="delete_user.php?id=<?php echo $donnees['id'] ?>">supprimer</a></div>
        </div>

        <?php
    }?>
    </div>
<?php
    require_once('foot.php');
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
?>

