<?php

//connexion à la bdd
try {
//    Connexion en local
    $bdd = new PDO('mysql:host=localhost;dbname=fake_news;charset=utf8;', 'root', '',
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}

catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}