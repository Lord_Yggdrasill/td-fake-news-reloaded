<?php session_start(); ?>
<?php require_once ('head.php') ?>
<div class="my-modal hidden">
    <div class="my-modal-content">
        <p>On vous avait prévenu qu'il allait faire tout noir... <br>
        Votre message en a profité pour s'enfuir jusqu'à chez nous !</p>
        <a class="modal-button expand" href="#"><i class="fa fa-check i-style"></i>J'ai peur du noir :'( </a>
    </div>
</div>
<div class="container">
    <?php require_once ('bdd_connexion.php');
    require ('header.php');
    $req = $bdd->query("SELECT * FROM settings WHERE `name` = 'Accueil'");
    $resultat = $req->fetch();
    ?>

    <main>
        <div class="down-border">
            <h1>Fake news II</h1>
            <p class="uppercase">
                <?php
                    if ($resultat) {
                        echo $resultat['value'];
                    } else {
                        echo '';
                    }
                ?></p>
        </div>

            <h2 class="up-border">Les dernières <span class="font-weight-bold">fake news </span>!</h2>

        <div class="row">
            <?php

            $req = $bdd->query('SELECT * FROM posts ORDER BY date_creation DESC LIMIT 0, 3');
            while ($donnees = $req->fetch()) {?>
                <div class="livre col-md-4">
                    <div>
                        <img src="images/<?php echo $donnees['image'] ?>" alt="Photo de livres"
                             srcset="images/<?php echo $donnees['image'] ?> 368w"
                             sizes="(max-width: 768px) 300px,
                        (max-width: 991px) 200px,
                        (max-width: 1199px) 280px,
                        (min-width: 1200px) 320px">
                    </div>
                    
                    <div class="date-news"><?php echo $donnees['date_creation'] ?></div>

                    <h3><?php echo $donnees['title'] ?></h3>
                    <p><?php echo $donnees['chapo'] ?></p>
                </div>
                <?php
            }
            ?>
            
        </div>

        <a class="expand" href="trucs_en_toc.php"><i class="fa fa-file i-style"></i>J'en veux encore !</a>

        <div class="banner-border">
            <div class="banner">
                <p>"On peut tromper <span class="font-weight-bold">une</span> fois
                    <span class="font-weight-bold">mille</span> personnes,
                mais on ne peut pas tromper <span class="font-weight-bold">mille</span> fois
                    <span class="font-weight-bold">une</span> personne." - <span class="author">Émile</span></p>
            </div>
        </div>

<!--        <section>-->
<!--            <div class="row">-->
<!--                <div class="col-md-8">-->
<!--                <h2>C'est encore un coup de la CIA !</h2>-->
<!--                    <div>-->
<!--                        <img src="images/pic04.jpg" alt="Photo de dossiers confidentiels"-->
<!--                             srcset="images/pic04.jpg 784w"-->
<!--                             sizes="(max-width: 768px) 180px,-->
<!--                                 (max-width: 991px) 350px,-->
<!--                                 (max-width: 1199px) 600px,-->
<!--                                 (min-width: 1200px) 700px">-->
<!--                    </div>-->
<!---->
<!--                    <h3>Des faux passeports pour les aliens</h3>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste iusto labore nemo quaerat-->
<!--                        quasi ullam vitae? Atque consectetur dignissimos facere facilis labore magnam molestias,-->
<!--                        nam, non obcaecati quidem quod voluptates?</p>-->
<!---->
<!--                    <a class="expand down-border" href="#"><i class="fa fa-file i-style"></i>Je veux tout savoir</a>-->
<!---->
<!--                    <div class="gray-border"></div>-->
<!---->
<!---->
<!--                <h2 class="up-border">À moins que ce ne soit la SPA ?</h2>-->
<!---->
<!--                    <div>-->
<!--                        <img src="images/pic05.jpg" alt="Photo de tourne-disque"-->
<!--                             srcset="images/pic05.jpg 784w"-->
<!--                             sizes="(max-width: 768px) 180px,-->
<!--                                 (max-width: 991px) 350px,-->
<!--                                 (max-width: 1199px) 600px,-->
<!--                                 (min-width: 1200px) 700px">-->
<!--                    </div>-->
<!---->
<!--                    <h3>Des aliens déguisés en hamsters</h3>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at cumque dignissimos facere-->
<!--                        impedit, ipsam, minus porro quam quo sit soluta unde veritatis voluptate. Est harum incidunt nisi-->
<!--                        pariatur qui.</p>-->
<!---->
<!--                    <a class="expand" href="#"><i class="fa fa-file i-style"></i>J'adopte un alien</a>-->
<!--                </div>-->
<!---->
<!--                <div class="col-md-4">-->
<!--                    <div class="post-border">-->
<!--                        <div class="date-news">-->
<!--                            30 juillet-->
<!--                        </div>-->
<!--                        <h3>Post à souder</h3>-->
<!--                        <p>L'actualité brûlante du jour. Vous resterez collés à vous sièges.</p>-->
<!--                    </div>-->
<!---->
<!--                    <div class="post-border post-top">-->
<!--                        <div class="date-news">-->
<!--                            28 juillet-->
<!--                        </div>-->
<!--                        <h3>Post de police</h3>-->
<!--                        <p>Hé là ! Qui va là ? Veuillez présenter les documents afférents à la conduite de votre véhicule.</p>-->
<!--                    </div>-->
<!---->
<!--                    <div class="post-border post-top">-->
<!--                        <div class="date-news">-->
<!--                            2 juillet-->
<!--                        </div>-->
<!--                        <h3>Post apocalyptique</h3>-->
<!--                        <p>! ecnegru ne etsicroxe nu à leppa setiaf ,icec eril à zevirra suov iS</p>-->
<!--                    </div>-->
<!---->
<!---->
<!--                    <h3 class="up-border">Offre geofitness</h3>-->
<!--                    <div class="float-style">-->
<!--                        <div class="float-left float-md-none">-->
<!--                            <img src="images/pic06.jpg" alt="Photo d'un globe terrestre'"-->
<!--                                 srcset="images/pic06.jpg 180w"-->
<!--                                 sizes="(max-width: 768px) 100px,-->
<!--                                     (max-width: 991px) 100px,-->
<!--                                     (max-width: 1199px) 150px,-->
<!--                                     (min-width: 1200px) 170px">-->
<!--                        </div>-->
<!--                            <p>Incroyable ! Grâce à GeoFitness, j'apprends la géographie tout en faisant de-->
<!--                            l'exercice ! Seulement 34999,99€ ! Le 1er acheté, le 2ème à <span class="font-weight-bold">+500%</span> <br>-->
<!--                            Offre exclusive reservée au 2 premiers pigeons !</p>-->
<!---->
<!--                            <a class="expand" href=""><i class="fa fa-file i-style"></i>Je suis un pigeon !</a>-->
<!--                    </div>-->
<!---->
<!--                    <h3 class="up-border">La plante vampire</h3>-->
<!--                    <div class="float-style">-->
<!--                        <div class="float-left float-md-none">-->
<!--                            <img src="images/pic07.jpg" alt="Photo d'une fleur rougeâtre'"-->
<!--                                 srcset="images/pic07.jpg 180w"-->
<!--                                 sizes="(max-width: 768px) 100px,-->
<!--                                     (max-width: 991px) 100px,-->
<!--                                     (max-width: 1199px) 150px,-->
<!--                                     (min-width: 1200px) 170px">-->
<!--                        </div>-->
<!--                        <p class="padding-align">Incroyable ! Si vous touchez ses feuilles, cette plante vous videra de votre sang-->
<!--                        en un clin d'œil. <br>-->
<!--                        Le cadeau empoisonné idéal !</p>-->
<!---->
<!--                        <a class="expand" href="#"><i class="fa fa-file i-style"></i>Maman j'ai peur !</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </section>-->
    </main>


</div>
<?php require ('footer.php')?>
<?php require ('foot.php')?>
