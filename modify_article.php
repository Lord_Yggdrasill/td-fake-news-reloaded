<?php require_once ('bdd_connexion.php');
session_start();
require_once ('head.php');
if (isset($_SESSION['pseudo']) && $_SESSION['role'] == 'admin' ||
    isset($_SESSION['pseudo']) && $_SESSION['role'] == 'redactor') {?>
    <h2>Modifier un utilisateur</h2>
    <?php
//Je récupère l'article concerné par la modif' en allant chercher dans la BDD l'id que j'ai dans le GET
    $current_article = $bdd->prepare('SELECT * FROM posts WHERE id = :id');
    $current_article->execute(array(
        'id' => $_GET['id']
    ));

    while ($donnees = $current_article->fetch()) {
        ?> <p>Vous modifiez l'article : <span class="uppercase font-weight-bold"><?php echo $donnees['title'];?></span></p>
        <form action="" method="post">
        <label for="modify_title">Saisir vos modifications du titre : </label><br>
        <input type="text" id="modify_title" name="modify_title" value="<?php echo $donnees['title'];?>"><br>
        <label for="modify_chapo">Saisir vos modifications du chapo : </label><br>
        <textarea name="modify_chapo" id="modify_chapo" cols="40" rows="10">
        <?php echo $donnees['chapo'] ?>
    </textarea><br>
        <label for="modify_content">Saisir vos modifications du contenu : </label><br>
        <textarea name="modify_content" id="modify_content" cols="50" rows="20">
        <?php echo $donnees['content'] ?>
    </textarea><br>
        <label for="modify_img">Nouvelle image : </label><br>
        <input type="text" id="modify_img" name="modify_img" value="<?php echo $donnees['image'];?>"><br>
        <input class="expand" type="submit" value="Valider les modifications">
        </form><?php
    }
    ?>



    <?php
    if ( !empty($_POST['modify_title']) && !empty($_POST['modify_chapo']) && !empty($_POST['modify_content']) && !empty($_POST['modify_img'])) {
        $modify_article = $bdd->prepare('UPDATE posts SET `title` = :title, chapo = :chapo, `content` = :content, `image` = :image
                                            WHERE id = :id');
        $modify_article->execute(array(
            'title' => $_POST['modify_title'],
            'chapo' => $_POST['modify_chapo'],
            'content' => $_POST['modify_content'],
            'image' => $_POST['modify_img'],
            'id' => $_GET['id']
        ));
        echo 'L\'article a bien été modifié !';
    }
    ?>

    <div>
        <a class="expand" href="articles.php">Retour à la liste des articles</a>
    </div>

    <?php
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
?>

