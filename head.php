<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/fontawesome-all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@700&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400;1,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script type="application/javascript" src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script type="application/javascript" src="vendor/jquery/popper.min.js"></script>
    <script type="application/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/cf25642e21.js" crossorigin="anonymous"></script>
    <title>Fake News II, Reloaded</title>
</head>
<body>