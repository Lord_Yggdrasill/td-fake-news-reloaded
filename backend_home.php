<?php require_once ('bdd_connexion.php');
session_start();
//si la case 'rester connecté" est cochée, je set les cookies pour le login et le mdp (ne pas stocker le mdp en temps normal)
if (isset($_POST['perma_co'])) {
    setcookie('login', $_POST['pseudo'], time() + 3600*24*365, null, null, false, true); //cookie qui stocke le pseudo
    setcookie('pass_hash', hash('sha512', $_POST['pw']), time() + 3600*24*365, null, null, false, true); //cookie qui stocke le mdp hashé
}
require_once ('backend_nav.php');
?>

<div class="container">
    <?php
//Si les POST pseudo et pw existent :
if (isset($_POST['pseudo']) && isset($_POST['pw'])) {
    //si les champs ne sont pas vides
    if (!empty($_POST['pseudo'] && !empty($_POST['pw']))) {
        //je prépare une requête pour récupérer le pseudo posté, et le comparer à la bdd user
        $req = $bdd->prepare('SELECT * FROM users WHERE login = :pseudo');
        $req->execute(array(
            'pseudo' => $_POST['pseudo']
        ));

        //je parcours la bdd que et je met la requête dans la variable resultat
        $resultat = $req->fetch();


        //s'il n'y a pas la variable résultat on affiche un message
        if (!$resultat) {
            echo 'Mauvais identifiant ou mot de passe';
            //on hashe le mdp posté, et s'il correspond à celui de la bdd,
            //on stocke l'id et le pseudo dans la SESSION
        } elseif (hash('sha512', $_POST['pw']) === $resultat['password']) {
            $_SESSION['id'] = $resultat['id'];
            $_SESSION['pseudo'] = $_POST['pseudo'];
            //si l'user a un rôle admin :
            if ($resultat['role'] == 'admin') {
                $_SESSION['role'] = 'admin';

                //sinon si l'user a un rôle redacteur :
                } elseif ($resultat['role'] == 'redactor') {
                $_SESSION['role'] = 'redactor';
            }
            echo 'Bienvenue ' . strip_tags($_SESSION['pseudo']);
        } else {
            echo 'Mauvais  mot de passe';
        }
    } else {
        echo 'Veuillez renseigner tous les champs';
    }
}


?>
</div>

<div class="row">
    <div class="col-md-12">
        <h1>Statistiques du site</h1>
        <p>Nombre total d'utilisateurs : <?php
            //Si l'user est un admin, j'affiche le compteur du nombre d'utilisateurs présents dans la BDD
            if ($_SESSION['role'] == 'admin') {
                $count_users = $bdd->query('SELECT COUNT(*) AS nb_users FROM users');
                $total_users = $count_users->fetch();
                echo $total_users['nb_users'];
            }
            ?></p>
        <p>Nombre total d'articles : <?php
            //J'affiche le compteur du nombre total d'articles
            $count_articles = $bdd->query('SELECT COUNT(*) AS nb_articles FROM posts');
            $total_articles = $count_articles->fetch();
            echo $total_articles['nb_articles'];
            ?></p>
    </div>
</div>
<?php
require_once('foot.php');
