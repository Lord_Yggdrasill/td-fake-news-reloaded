<?php require_once ('bdd_connexion.php');
session_start();
require_once ('head.php');

if (isset($_SESSION['pseudo'])&& $_SESSION['role'] == 'admin') {?>
    <h2>Ajouter un utilisateur :</h2>
<div class="container">
    <div class="row">
        <div class="form-group m-auto pt-3">
            <form action="" method="post">
                <input class="form-control" type="text" id="new_user" name="new_user" placeholder="Saisir un nom d'utilisateur"><br>
                <input class="form-control" type="password" id="new_user" name="new_password" placeholder="Saisir un mot de passe"><br>
                <label for="role">Saisir le role du nouvel utilisateur : </label><br>
                <select name="role" id="role">
                    <option value="admin">Administrateur</option>
                    <option value="redactor">Redacteur</option>
                </select><br>
                <input class="expand" type="submit" value="Ajouter">
            </form>
        </div>
    </div>
</div>

    <?php


    if (!empty($_POST['new_user']) && !empty($_POST['new_password']) && !empty($_POST['role'])) {
        //Je fais une requête pour vérifier que le pseudo n'est pas déjà présent dans la BDD :
        $req_pseudo = $bdd->prepare('SELECT login FROM users WHERE login = :pseudo');
        $req_pseudo->execute(array(
            'pseudo' => $_POST['new_user']));
        if ($req_pseudo->fetch()) {
            echo 'Utilisateur déjà existant';
        } else {
            //si le pseudo n'est pas déjà présent dans la BDD, alors je prépare une requête pour insérer
            //le pseudo, mdp et role postés via le formulaire
            $pw_hash = hash('sha512', $_POST['new_password']);
            $add_user = $bdd->prepare('INSERT INTO users (`login`, `password`, `role`) VALUES (:pseudo, :pw, :grade) ');
            $add_user->execute(array(
                'pseudo' => $_POST['new_user'],
                'pw' => $pw_hash,
                'grade' => $_POST['role']
            ));
            echo 'L\'utilisateur a bien été ajouté !';
        }
    }
    ?>

    <div>
        <a class="expand" href="users.php">Retour au panneau utilisateurs</a>
    </div>
<?php
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
?>


