<?php session_start();?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/fontawesome-all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Arvo:wght@700&family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;1,300;1,400;1,600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script type="application/javascript" src="vendor/jquery/jquery-3.5.1.min.js"></script>
    <script type="application/javascript" src="vendor/jquery/popper.min.js"></script>
    <script type="application/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/cf25642e21.js" crossorigin="anonymous"></script>
    <title>Trucs en Toc - Fake News II</title>
</head>
<body>
<div class="container">
<?php require_once ('bdd_connexion.php');
require 'header.php';
    $req = $bdd->query("SELECT * FROM settings WHERE `name` = 'trucs_en_toc'");
    $resultat = $req->fetch();
    ?>

    <div class="down-border">
        <p class="titre_style">Fake news II</p>
        <h1 class="uppercase title-reset">Trucs en toc</h1>
        <p class="uppercase"><?php
            if ($resultat) {
                echo $resultat['value'];
            } else {
                echo '';
            }
            ?></p>
    </div>

    <div class="row">
        <?php

        $req = $bdd->query('SELECT * FROM posts ORDER BY date_creation DESC');
        while ($donnees = $req->fetch()) {?>
            <div class="livre col-md-12">
                <div class="up-border"></div>
                <div>
                    <img src="images/<?php echo $donnees['image'] ?>" alt="Photo de livres"
                         srcset="images/<?php echo $donnees['image'] ?> 368w"
                         sizes="(max-width: 768px) 300px,
                            (max-width: 991px) 200px,
                            (max-width: 1199px) 280px,
                            (min-width: 1200px) 320px">
                </div>

                <div class="date-news"><?php echo $donnees['date_creation'] ?></div>

                <h3><?php echo $donnees['title'] ?></h3>
                <p><?php echo $donnees['chapo'] ?></p>
                <a class="expand" href="detail_article.php?id=<?php echo $donnees['id'] ?>"><i class="fa fa-file i-style"></i>Je veux la suite !</a>
            </div>
            <?php
        }
        ?>

    </div>
</div>

<?php
require 'footer.php';
require 'foot.php';
