<?php require_once ('bdd_connexion.php');
session_start();

if (isset($_SESSION['pseudo'])&& $_SESSION['role'] == 'admin') {
    if ($_SESSION['id'] != $_GET['id']) {
    $delete_user = $bdd->prepare('DELETE FROM users WHERE id = :id');
    $delete_user->execute(array(
    'id' => $_GET['id']
    ));
    header('Location: users.php');
    } else {
    echo 'Vous ne pouvez pas vous supprimer vous-même ! Vous allez être redirigés sur la liste...';
    header( "Refresh:5; url=users.php", true, 303);
    }
} else {
    require_once ('head.php');
    echo "Vous n'ête pas autorisés à accéder à cette page";
}
