-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 03 sep. 2020 à 00:06
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `fake_news`
--

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `chapo` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `date_creation`, `title`, `chapo`, `content`, `image`) VALUES
(1, '2020-08-21 19:00:00', 'comment ranger un livre ?', 'On vous ment <span class=\"font-weight-bold\">depuis le début</span>, il faut ranger les livres sur la tranche, c\'est meilleur pour leur santé mentale.\r\nLe témoignage exclusif de <a href=\"#\">Robert, dictionnaire de français</a>.', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ut dictum sem. Vivamus sed metus metus. Fusce sit amet urna quam. Nunc augue est, iaculis vel congue vel, egestas a orci. Etiam vel ligula egestas, lobortis lectus at, feugiat massa. Nulla accumsan placerat sem, sit amet auctor elit vulputate ut. Ut consectetur libero a ornare hendrerit. Pellentesque iaculis pellentesque dolor, id venenatis diam gravida et.\r\n\r\nFusce feugiat lacinia leo in venenatis. Curabitur a commodo ipsum, in rhoncus mi. Donec dapibus, risus ac fermentum malesuada, nunc sem sollicitudin felis, sed pulvinar metus eros a tellus. Aenean sed ligula ipsum. Vestibulum ligula metus, tempus venenatis ultricies sit amet, gravida ac libero. Praesent in tellus eget elit suscipit scelerisque quis sed tellus. Donec ullamcorper maximus faucibus.\r\n\r\nCurabitur cursus enim sit amet aliquet lobortis. Nam ornare commodo orci hendrerit semper. Nulla facilisi. Pellentesque turpis eros, convallis ac nisl a, lacinia pharetra sapien. Maecenas volutpat, tellus in semper dapibus, sapien ligula ornare felis, ut lacinia ipsum odio pharetra purus. Sed sollicitudin lacus sed lectus feugiat sodales. Donec tempus auctor interdum. Mauris vulputate turpis in nisl interdum, nec condimentum turpis dignissim. Integer sollicitudin sollicitudin nisl lobortis auctor. Integer ullamcorper arcu eget luctus tempor. Nunc quis erat enim. Praesent suscipit leo lectus, sit amet mattis tellus placerat sed. Nunc porttitor sed nisl quis laoreet.\r\n\r\nVestibulum ut sapien quis lacus condimentum sagittis. Ut odio nibh, bibendum vel ipsum in, porta vulputate justo. Vivamus condimentum arcu et orci congue consectetur a a turpis. Fusce dictum eros sed nunc fermentum malesuada. Vestibulum eros felis, iaculis ac hendrerit vitae, fermentum vitae ipsum. Cras placerat elit in lorem viverra aliquet. Sed efficitur nunc eros, sit amet egestas dolor interdum eu. Sed sit amet tincidunt mauris. Duis vitae est vitae elit laoreet semper sit amet sit amet eros. Maecenas hendrerit, ante ut semper condimentum, felis lectus dignissim libero, eu sagittis dui tellus vitae urna. Morbi neque neque, tincidunt sit amet urna a, aliquet aliquam urna. Nulla ut ligula id diam volutpat placerat. Aenean vitae odio eget sapien dictum mollis. Nulla nec arcu nec libero finibus gravida.\r\n\r\nNulla iaculis magna eget lectus molestie, at eleifend leo accumsan. Pellentesque ac varius augue. Vivamus sagittis ullamcorper lacus, quis pulvinar magna fringilla non. Fusce iaculis nibh sed felis commodo, vitae malesuada ipsum porttitor. In auctor varius ipsum, eget scelerisque massa fermentum lacinia. Phasellus luctus malesuada augue, a commodo ex tempor vitae. Nulla facilisi. Ut ut ex dignissim mi malesuada iaculis. Mauris id elit sit amet lacus porta laoreet ac in nulla. Donec vel scelerisque elit, nec interdum odio. Fusce hendrerit placerat leo a lobortis. Sed dapibus dui a metus iaculis viverra. ', 'pic01.jpg'),
(2, '2020-08-21 19:15:00', 'huile de palmipède', 'Des chercheurs ont découvert qu\'à cause de l\'huile de palme qu\'elle contient, une consommation excessive de pâte à tartiner provoquerait une mutation du pied en patte de canard. <a href=\"#\">Les photos exclusives ici !</a>', '\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum diam vel dolor dapibus, at pellentesque mi dignissim. Proin imperdiet ipsum vel sapien ultrices, id ullamcorper turpis facilisis. Phasellus faucibus urna molestie sem placerat, nec lobortis justo fringilla. Donec dui arcu, consequat eget nunc non, eleifend euismod justo. Proin a quam sit amet diam consectetur commodo. Etiam dignissim viverra placerat. Aenean diam felis, tristique eget tincidunt vitae, facilisis elementum odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt dui id metus auctor, ut accumsan nunc faucibus. Mauris pellentesque metus eu urna mattis, sed imperdiet libero viverra. Nulla non nisi risus.\r\n\r\nMorbi bibendum fringilla semper. Vivamus ex mi, sollicitudin nec augue eu, tempor tempor urna. Morbi eget malesuada lacus. Ut auctor nibh in nulla tincidunt, in consequat nisl auctor. Donec et arcu in augue bibendum consectetur sed non quam. Aliquam sodales, felis lacinia pharetra malesuada, enim tellus ultricies est, et accumsan erat eros et mi. Cras consectetur efficitur turpis eu ornare. Suspendisse gravida, quam ut sodales tristique, turpis mi pretium arcu, vitae eleifend dui ipsum nec lectus. Nulla faucibus, enim in iaculis placerat, mi lectus blandit enim, et sagittis ex sapien sit amet arcu. Morbi et rutrum dolor. Duis convallis augue at diam iaculis, eu ultricies arcu malesuada.\r\n\r\nSuspendisse id fringilla velit. Pellentesque tempor quis nibh eu auctor. Aliquam cursus sem a mauris viverra, sed gravida nunc tincidunt. Nam consequat vel est quis pharetra. Aliquam cursus arcu velit. Duis mattis fringilla metus, condimentum imperdiet diam. Pellentesque consequat lacus neque, porttitor sagittis magna viverra ac. Etiam ullamcorper vitae velit et lacinia.\r\n\r\nNulla malesuada vel eros ac interdum. Proin luctus tincidunt diam, eget vehicula nunc dictum in. Phasellus vitae auctor felis. Quisque pretium finibus nisl, sed pellentesque mi sollicitudin nec. Donec varius sem dapibus dolor rutrum, ut convallis velit euismod. Donec cursus sit amet ante vel tincidunt. Nulla sed rutrum turpis. Duis ut diam et velit vestibulum eleifend ac quis ex. ', 'pic02.jpg'),
(3, '2020-08-21 19:30:00', 'cerisier alien', '                                <span class=\"font-weight-bold\">EXCLUSIF !</span> Les aliens sont parmi nous ! Ils se cachent dans les cerisiers déguisés en fleurs. L\'interview exclusive de <a href=\"#\">Bob l\'extraterrestre.</a>                                ', '        \r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum diam vel dolor dapibus, at pellentesque mi dignissim. Proin imperdiet ipsum vel sapien ultrices, id ullamcorper turpis facilisis. Phasellus faucibus urna molestie sem placerat, nec lobortis justo fringilla. Donec dui arcu, consequat eget nunc non, eleifend euismod justo. Proin a quam sit amet diam consectetur commodo. Etiam dignissim viverra placerat. Aenean diam felis, tristique eget tincidunt vitae, facilisis elementum odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt dui id metus auctor, ut accumsan nunc faucibus. Mauris pellentesque metus eu urna mattis, sed imperdiet libero viverra. Nulla non nisi risus.\r\n\r\nMorbi bibendum fringilla semper. Vivamus ex mi, sollicitudin nec augue eu, tempor tempor urna. Morbi eget malesuada lacus. Ut auctor nibh in nulla tincidunt, in consequat nisl auctor. Donec et arcu in augue bibendum consectetur sed non quam. Aliquam sodales, felis lacinia pharetra malesuada, enim tellus ultricies est, et accumsan erat eros et mi. Cras consectetur efficitur turpis eu ornare. Suspendisse gravida, quam ut sodales tristique, turpis mi pretium arcu, vitae eleifend dui ipsum nec lectus. Nulla faucibus, enim in iaculis placerat, mi lectus blandit enim, et sagittis ex sapien sit amet arcu. Morbi et rutrum dolor. Duis convallis augue at diam iaculis, eu ultricies arcu malesuada.\r\n\r\nSuspendisse id fringilla velit. Pellentesque tempor quis nibh eu auctor. Aliquam cursus sem a mauris viverra, sed gravida nunc tincidunt. Nam consequat vel est quis pharetra. Aliquam cursus arcu velit. Duis mattis fringilla metus, condimentum imperdiet diam. Pellentesque consequat lacus neque, porttitor sagittis magna viverra ac. Etiam ullamcorper vitae velit et lacinia.\r\n\r\nNulla malesuada vel eros ac interdum. Proin luctus tincidunt diam, eget vehicula nunc dictum in. Phasellus vitae auctor felis. Quisque pretium finibus nisl, sed pellentesque mi sollicitudin nec. Donec varius sem dapibus dolor rutrum, ut convallis velit euismod. Donec cursus sit amet ante vel tincidunt. Nulla sed rutrum turpis. Duis ut diam et velit vestibulum eleifend ac quis ex.     ', 'pic01.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'accueil', 'Il revient et il est pas content ! <br>\r\nMythonné en PHP et MySql.                                                                                '),
(2, 'trucs_en_toc', 'Mais puisqu\'on vous dit que c\'est vrai !                                                                ');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `role`) VALUES
(1, 'florent', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin'),
(2, 'dupont', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'redactor'),
(3, 'test', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin'),
(4, 'jean', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'redactor'),
(5, 'yggdrasill', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin'),
(6, 'prof', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin'),
(13, 'doe', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin'),
(17, 'user2', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
