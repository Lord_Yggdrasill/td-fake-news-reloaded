    <footer>
        <h3 class="letter-space">Des questions ? <span class="font-weight-bold">Contactez-nous :</span></h3>

        <div class="row">
            <div class="col-md-6">
                <form action="" method="post">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input class="form-control" type="text" name="nom" id="nom" value="" placeholder="Nom">
                            <label for="nom"><span id="name-error"></span></label>
                        </div>
                        <div class="form-group col-md-6">
                            <input class="form-control" type="email" name="email" id="email" value="" placeholder="Email">
                            <label for="email"><span id="email-error"></span></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                        <label for="message"><span id="message-error"></span></label>
                    </div>
                    <button class="col-sm-12 col-md-9 expand" type="submit" id="form_submit" >
                        <i class="fa fa-envelope i-style"></i>Il va faire tout noir !</button>
                </form>
            </div>

            <div class="col-md-6">
                <p>Nous vous répondrons exclusivement par pigeon voyageur.</p>

                <div class="social-media">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-home fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <p>1234 Ch. de la perdition <br>
                                        457R6 Quelque-part <br>
                                        Jupiter</p>
                                </div>
                            </div>

                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <p>(0_O) \0_\0/ _0/ (T_T)</p>
                                </div>
                            </div>

                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <a href="#">ne-pas-repondre@fake-news.info</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <a href="#">@fakenews</a>
                                </div>
                            </div>

                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <a href=#>instagram.com/fake-news</a>
                                </div>
                            </div>

                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-dribbble fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <a href="#">dribbble.com/fake-news</a>
                                </div>
                            </div>

                            <div class="flex-media">
                                <div>
                                    <span class="fa-stack fa-2x float-left">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                    </span>
                                </div>
                                <div>
                                    <a href="#">facebook.com/fake-news</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
