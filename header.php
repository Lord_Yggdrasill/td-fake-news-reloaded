<header>
    <button id="burger-toggle"><i class="fa fa-bars"></i></button>

    <nav id="nav-menu" class="hidden col-6 col-sm-4 col-md-4 col-lg-12">
        <ul>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-home fa-stack-1x fa-inverse"></i>
                </span>
                <a href="index.php">Rembobiner</a>
            </li>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-chart-bar fa-stack-1x fa-inverse"></i>
                </span>
                <a href="trucs_en_toc.php" data-toggle="popover" data-placement="bottom" data-content=""  >Trucs en toc</a>
<!--                <ul class="ul-popover">-->
<!--                    <li>-->
<!--                        <a href="#">Faux</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Contre-vérités</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#">Erreurs</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="#" data-toggle="popover2"  data-content=""  >Falsifications</a>-->
<!--                        <ul class="second-level">-->
<!--                            <li>-->
<!--                                <a href="#">Faux liens</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="#">Fausses tabulations</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="#">Faux titres</a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <ul class="ul-popover">-->
<!--                    <li>-->
<!--                        <a href="#">Non</a>-->
<!--                    </li>-->
<!--                </ul>-->
            </li>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-cog fa-stack-1x fa-inverse"></i>
                </span>
                <a href="connexion.php">Rouages</a>
            </li>
        </ul>
    </nav>
</header>