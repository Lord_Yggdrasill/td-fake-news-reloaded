<?php require_once ('bdd_connexion.php');
session_start();
if (isset($_SESSION['pseudo'])) {
    header('Location: backend_home.php');
} else {
require_once 'head.php';
    ?>
<!--formulaire de connexion-->
<div class="container">
    <div class="row">
        <div class="form-group m-auto pt-3">
            <form action="backend_home.php" method="post">
<!--                <label for="pseudo" class="backend_label">Pseudo : </label>-->
                <input type="text" name="pseudo" class="form-control" placeholder="Pseudo"><br>
<!--                <label for="pw" class="backend_label">Mot de passe : </label>-->
                <input type="password" name="pw" class="form-control" placeholder="Mot de passe"><br>
                <div class="flex-media pb-2">
                <label for="perma_co" class="backend_label">Rester connecté </label>
                    <input type="checkbox" name="perma_co" id="perma_co" checked="checked" class="form-control">
                </div>
                    <input type="submit" name="connexion" value="Connexion" class="expand">
            </form>
        <div>
    </div>
</div>
<?php
}
?>
