//Chargement du DOM
$(document).ready(function () {
    init();
})

//Regroupement des fonctions quand le DOM est OK
function init() {
    showMenu();
    // popoverUl();
    // subMenu();
    testInputForm();
    checkInputForm();
    hideModal();
}

//Fonction du menu burger
function showMenu () {
    // Au clic, j'affiche/cache le menu déroulant en faisant un toggle sur la classe "hidden"
    $('#burger-toggle').unbind('click').on('click', function () {
        $('#nav-menu').toggleClass('hidden');
        //Je switche entre les classes des deux icônes, avec une légère animation de disparition/réapparition
        $('#burger-toggle i').toggleClass('fa-bars fa-times').animate({opacity: 0}, 1).animate({opacity: 1}, 500);
    })
}

//Popover du 1er sous-menu
// function popoverUl() {
//     $('[data-toggle="popover"]').popover({
//         html: true,
//         trigger: 'click',
//         content: $('.ul-popover')
//     })
// }
//
// //Popover du 2ème sous-menu
// function subMenu() {
//     $('[data-toggle="popover"]').on('click', function () {
//         //Je rappelle le listener pour qu'il reste également sur le 1er popover
//         $('[data-toggle="popover2"]').popover({
//             html: true,
//             placement: "right",
//             trigger: 'focus',
//             //J'enlève la class="arrow" du template du popover
//             template: '<div class="popover2" role="tooltip"><div></div>' +
//                 '<h3 class="popover-header">' +
//                 '</h3><div class="popover-body"></div></div>',
//             content: $('.second-level'),
//         })
//     })
// }

//*****Formulaire*****
function hideModal() {
    $('.modal-button').on('click', function () {
        $('.my-modal').addClass('hidden');
    })
}

//Je récupère les inputs grâce à leur id et je les mets dans les variables
let nom = $('#nom');
let email = $('#email');
let message = $('#message');

const nomRegex = new RegExp("^[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])+))*$");
const emailRegex = new RegExp(/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/);


function testInputForm() {
    $('#form_submit').on('click', function () {
        //Au clic, je recupère la valeur de "nom" que je mets dans une variable "nomValue"
        let nomValue = nom.val();
        //Si la valeur est entre 2 et 40 caractères, et qu'elle correspond à la regex
        if (nomValue.length>2 && nomValue.length<40 && nomRegex.test(nomValue)) {
            //Je vide le champ qui affiche l'erreur, j'ajoute la classe valide et j'enlève l'invalide
            $('#name-error').text('');
            $(nom).addClass('is-valid').removeClass('is-invalid');
        } else {
            //Si la condition n'est pas remplie, j'affiche le texte d'erreur et je vide mon champ input
            //pour que l'internaute puisse re remplir l'input
            $('#name-error').text('Les lettres anonymes, c\'est mal !');
            $(nom).addClass('is-invalid').val('');
        }

        let emailValue = email.val();
        if (emailValue.length>2 && emailValue.length<60 && emailRegex.test(emailValue)) {
            $('#email-error').text('');
            $(email).addClass('is-valid').removeClass('is-invalid');
        } else {
            $('#email-error').text('Et comment on fait pour vous spammer ?');
            $(email).addClass('is-invalid').val('');
        }

        let messageValue = message.val();
        if (messageValue) {
            $('#message-error').text('');
            $(message).addClass('is-valid').removeClass('is-invalid');
        } else {
            $('#message-error').text('Non mais allô quoi, tu nous écris ou tu nous écris rien ?');
            $(message).addClass('is-invalid').val('');
        }

        //Si les conditions de tous les champs sont respectées
        if (nomValue.length>2 && nomValue.length<40 && nomRegex.test(nomValue)
            && emailValue.length>2 && emailValue.length<60 && emailRegex.test(emailValue)
            && messageValue) {
            //J'enlève la classe qui cache ma modale
            $('.my-modal').removeClass('hidden');
        }
    })
}

function checkInputForm () {
    //Je vérifie chaque input de l'internaute
    $(nom).on('input', function () {
        let nomValue = nom.val();
        if (nomValue.length > 2 && nomValue.length < 40 && nomRegex.test(nomValue)) {
            //Si ses inputs remplissent mes conditions, j'enlève la classe "is-invalid" (cadre rouge) et j'ajoute la
            //classe "is-valid" (cadre vert)
            $('#nom').removeClass('is-invalid').addClass('is-valid');
        } else {
            //Sinon Je fais l'inverse
            $('#nom').removeClass('is-valid').addClass('is-invalid');
        }
    });

    $(email).on('input', function () {
        let emailValue = email.val();
        if (emailValue.length > 2 && emailValue.length < 60 && emailRegex.test(emailValue)) {
            $('#email').removeClass('is-invalid').addClass('is-valid');
        } else {
            $('#email').removeClass('is-valid').addClass('is-invalid');
        }
    });

    $(message).on('input', function () {
        let messageValue = message.val();
        if (messageValue) {
            $('#message').removeClass('is-invalid').addClass('is-valid');
        } else {
            $('#message').removeClass('is-valid').addClass('is-invalid');
        }
    });
}

//J'empêche le submit du form de recharger la page
$('form').on('submit', function (e) {
    e.preventDefault();
})
