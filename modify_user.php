<?php require_once ('bdd_connexion.php');
session_start();
require_once ('head.php');

?>
<h2>Modifier un utilisateur</h2>
<?php
//Je récupère l'utilisateur concerné par la modif' en allant chercher dans la BDD l'id que j'ai dans le GET
$current_user = $bdd->prepare('SELECT id, login FROM users WHERE id = :id');
$current_user->execute(array(
'id' => $_GET['id']
));

while ($donnees = $current_user->fetch()) {
?> <p>Vous modifiez l'utilisateur : <?php echo $donnees['login'];?></p>
<?php
}
?>
<div class="container">
    <div class="row">
        <div class="form-group m-auto pt-3">
            <form action="" method="post">
                <input class="form-control" type="text" id="modify_user" name="modify_user" placeholder="Saisir un nouveau nom"><br>
                <input class="form-control" type="password" id="modify_user" name="modify_password" placeholder="Saisir un mot de passe"><br>
                <label for="role">Saisir le role de l'utilisateur : </label><br>
                <select name="role" id="role">
                    <option value="admin">Administrateur</option>
                    <option value="redactor">Redacteur</option>
                </select><br>
                <input class="expand" type="submit" value="Modifier">
            </form>
        </div>
    </div>
</div>

<?php

if (!empty($_POST['modify_user']) && !empty($_POST['modify_password']) && !empty($_POST['role'])) {
//Je fais une requête pour vérifier que le pseudo n'est pas déjà présent dans la BDD :
    $req_pseudo = $bdd->prepare('SELECT login FROM users WHERE login = :pseudo');
    $req_pseudo->execute(array(
        'pseudo' => $_POST['modify_user']));
    if ($req_pseudo->fetch()) {
        echo 'Utilisateur déjà existant';
    } else {
//je sais quel user modifier grâce à la clause WHERE qui cherche dans la BDD l'id que j'ai dans le GET
//l'id correspond donc à l'user que je veux modifier
        $pw_hash = hash('sha512', $_POST['modify_password']);
        $add_user = $bdd->prepare('UPDATE users SET `login` = :pseudo, password = :pw, `role` = :grade
                                            WHERE id = :id');
        $add_user->execute(array(
            'pseudo' => $_POST['modify_user'],
            'pw' => $pw_hash,
            'grade' => $_POST['role'],
            'id' => $_GET['id']
        ));
        echo 'L\'utilisateur a bien été modifié !';
    }
}
?>

<div>
    <a class="expand" href="users.php">Retour au panneau utilisateurs</a>
</div>

