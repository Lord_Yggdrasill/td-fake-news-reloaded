<?php require_once ('bdd_connexion.php');
session_start();
require_once 'backend_nav.php';
require_once ('head.php');
if (isset($_SESSION['pseudo']) && $_SESSION['role'] == 'admin' ||
    isset($_SESSION['pseudo']) && $_SESSION['role'] == 'redactor') {?>
    <a class="expand" href="add_article.php">Ajouter un article</a>
    <?php
    $req = $bdd->query('SELECT * FROM posts');
    ?>
    <h2>Liste des articles : </h2>

    <div class="row">
        <?php

        $req = $bdd->query('SELECT * FROM posts ORDER BY date_creation DESC');
        while ($donnees = $req->fetch()) {?>
            <div class="livre col-md-4 articles-request">
                <div>
                    <img src="images/<?php echo $donnees['image'] ?>" alt="Photo de livres"
                         srcset="images/<?php echo $donnees['image'] ?> 368w"
                         sizes="(max-width: 768px) 300px,
                        (max-width: 991px) 200px,
                        (max-width: 1199px) 280px,
                        (min-width: 1200px) 320px">
                </div>

                <div class="date-news"><?php echo $donnees['date_creation'] ?></div>

                <h3><?php echo $donnees['title'] ?></h3>
                <p><?php echo $donnees['chapo'] ?></p>
                <a href="modify_article.php?id=<?php echo $donnees['id'] ?>">modifier</a> <a href="delete_article.php?id=<?php echo $donnees['id'] ?>">supprimer</a>
            </div>
            <?php
        }
        ?>

    </div>

    <?php
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
?>

