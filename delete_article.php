<?php require_once ('bdd_connexion.php');
session_start();
require_once ('head.php');

if (isset($_SESSION['pseudo']) && $_SESSION['role'] == 'admin' ||
    isset($_SESSION['pseudo']) && $_SESSION['role'] == 'redactor') {
    if (isset($_GET['id'])) {
        $delete_articles = $bdd->prepare('DELETE FROM posts WHERE id = :id');
        $delete_articles->execute(array(
            'id' => $_GET['id']
        ));
        header('Location: articles.php');
    } else {
        echo 'Erreur lors de la suppression de  l\'article ! Vous allez être redirigés sur la liste...';
        header( "Refresh:5; url=articles.php", true, 303);
    }
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
