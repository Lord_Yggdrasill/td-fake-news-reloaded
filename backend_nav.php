<?php require_once ('bdd_connexion.php');
if (!isset($_SESSION['pseudo'])) {
    header('Location:connexion.php');
}
require_once ('head.php');?>
<header>
    <button id="burger-toggle"><i class="fa fa-bars"></i></button>

    <nav id="nav-menu" class="hidden col-6 col-sm-4 col-md-4 col-lg-12">
        <ul>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-home fa-stack-1x fa-inverse"></i>
                </span>
                <a href="index.php">Accueil</a>
            </li>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-cog fa-stack-1x fa-inverse"></i>
                </span>
                <a href="settings.php">Réglages</a>
            </li>

            <!--Si l'user a le rôle admin, j'affiche l'onglet "utilisateurs"-->
            <?php if ($_SESSION['role'] == 'admin') {?>
            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                </span>
                <a href="users.php">Utilisateurs</a>
            </li>

            <?php } ?>

            <li>
                <span class="fa-stack fa-2x">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fa fa-file-text-o fa-stack-1x fa-inverse"></i>
                </span>
                <a href="articles.php">Articles</a>
            </li>

            <li>
            <span class="fa-stack fa-2x">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fa fa-sign-out fa-stack-1x fa-inverse"></i>
            </span>
                <a href="deconnexion.php">Déconnexion</a>
            </li>
        </ul>
    </nav>
</header>