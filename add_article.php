<?php require_once ('bdd_connexion.php');
session_start();
require_once ('head.php');
if (isset($_SESSION['pseudo']) && $_SESSION['role'] == 'admin' ||
    isset($_SESSION['pseudo']) && $_SESSION['role'] == 'redactor') {?>
    <h2>Ajouter un article</h2>

    <form action="" method="post">
        <label for="modify_title">Saisir un titre : </label><br>
        <input type="text" id="modify_title" name="modify_title"><br>
        <label for="modify_chapo">Saisir un chapo : </label><br>
        <textarea name="modify_chapo" id="modify_chapo" cols="40" rows="10">

    </textarea><br>
        <label for="role">Saisir un contenu : </label><br>
        <textarea name="modify_content" id="modify_content" cols="50" rows="20">

    </textarea><br>
        <label for="modify_img">Saisir le nom de l'image : </label><br>
        <input type="text" id="modify_img" name="modify_img"><br>
        <input class="expand" type="submit" value="Valider les modifications">
    </form>

    <?php
    if ( !empty($_POST['modify_title']) && !empty($_POST['modify_chapo']) && !empty($_POST['modify_content']) && !empty($_POST['modify_img'])) {
        $modify_article = $bdd->prepare('INSERT INTO posts (`date_creation`, `title`, `chapo`, `content`, `image`)
    VALUES (:date_creation, :title, :chapo, :content, :image)');
        $modify_article->execute(array(
            'date_creation' => date("Y-m-d H:i:s"),
            'title' => $_POST['modify_title'],
            'chapo' => $_POST['modify_chapo'],
            'content' => $_POST['modify_content'],
            'image' => $_POST['modify_img'],
        ));
        echo 'L\'article a bien été ajouté !';
    }
    ?>

    <div>
        <a class="expand" href="articles.php">Retour à la liste des articles</a>
    </div>
<?php
} else {
    echo "Vous n'êtes pas autorisé à accéder à cette page";
}
?>


