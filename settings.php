<?php require_once ('bdd_connexion.php');
session_start();
require_once 'backend_nav.php';
require_once ('head.php');
$req = $bdd->query("SELECT * FROM settings");
?>

    <form action="" method="post">
        <?php
            while ($donnees = $req->fetch()) {?>
                <label for="<?php echo $donnees['name'] ?>">Vous modifiez la page : <?php echo $donnees['name'] ?></label>
                <br>
                <textarea name="<?php echo $donnees['name'] ?>" id="<?php echo $donnees['name'] ?>" cols="30" rows="10">
                    <?php echo $donnees['value'] ?>
                </textarea><br>
                <?php
            }
        ?>
        <input class="expand" type="submit">
    </form>

<?php
if (isset($_POST['accueil']) && !empty($_POST['accueil']) ) {
    $modify_title = $bdd->prepare("UPDATE settings SET `value` = :new_value WHERE `name` = 'accueil' ");
    $modify_title->execute(array(
        'new_value' => $_POST['accueil'],
    ));
    echo 'Le titre de la page "Accueil" a bien été modifié !<br>';
}

if (isset($_POST['trucs_en_toc']) && !empty($_POST['trucs_en_toc']) ) {
    $modify_title = $bdd->prepare("UPDATE settings SET `value` = :new_value WHERE `name` = 'trucs_en_toc' ");
    $modify_title->execute(array(
        'new_value' => $_POST['trucs_en_toc'],
    ));
    echo 'Le titre de la page "Trucs en toc" a bien été modifié !';
}
//require_once 'foot.php';